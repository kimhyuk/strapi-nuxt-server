"use strict";

const _ = require("lodash");

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK] [YEAR (optional)]
 */

module.exports = {
  /**
   * Simple example.
   * Every monday at 1am.
   */
  "*/60 * * * * *": async () => {
    console.log("cron start!");
    const count = await strapi.plugins["users-permissions"]
      .queries("user", "users-permissions")
      .count();

    const users = await strapi.plugins["users-permissions"]
      .queries("user", "users-permissions")
      .find({
        _limit: count
      });

    _.forEach(users, user => {
      const results = _.filter(
        users,
        target =>
          target.referral === user.referralCode &&
          String(target.phone).search(/\+821[0-1]/i) !== -1
      );

      if (user.referralCount !== results.length) {
        strapi.plugins["users-permissions"]
          .queries("user", "users-permissions")
          .update({ _id: user._id }, { referralCount: results.length });
      }
    });

    console.log("cron");
  }
};
