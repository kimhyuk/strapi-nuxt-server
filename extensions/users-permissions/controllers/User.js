"use strict";

const _ = require("lodash");

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {
  show: async ctx => {
    const params = _.assign(ctx.request.body, {
      _sort: "referralCount:desc",
      _limit: 10
    });
    const users = await strapi.plugins["users-permissions"]
      .queries("user", "users-permissions")
      .find(params);

    const data = _.map(users, user => {
      const splits = user.email.split("@");
      const id = splits[0].slice(0, -2);
      const email = splits[1];
      return {
        email: `${id}**@${email}`,
        referralCount: user.referralCount
      };
    });
    ctx.send(data);
  },
  me: async ctx => {
    const user = ctx.state.user;

    if (!user) {
      return ctx.badRequest(null, [
        { messages: [{ id: "No authorization header was found" }] }
      ]);
    }

    const body = _.pick(ctx.request.body, ["address"]);

    const result = await strapi.plugins["users-permissions"].services.user.edit(
      { _id: user._id },
      body
    );

    ctx.state.user = result;

    // const result = await strapi.plugins["users-permissions"].queries.user.edit(
    //   { _id: user._id },
    //   ctx.body
    // );

    const data = _.omit(
      ctx.state.user.toJSON ? ctx.state.user.toJSON() : ctx.state.user,
      ["password", "resetPasswordToken"]
    );

    // Send 200 `ok`
    ctx.send(data);
  },
  edit: async ctx => {
    const user = ctx.state.user;

    if (!user) {
      return ctx.badRequest(null, [
        { messages: [{ id: "No authorization header was found" }] }
      ]);
    }

    const body = _.pick(ctx.request.body, ["address"]);

    const result = await strapi.plugins["users-permissions"].services.user.edit(
      { _id: user._id },
      body
    );

    ctx.state.user = result;

    // const result = await strapi.plugins["users-permissions"].queries.user.edit(
    //   { _id: user._id },
    //   ctx.body
    // );

    const data = _.omit(
      ctx.state.user.toJSON ? ctx.state.user.toJSON() : ctx.state.user,
      ["password", "resetPasswordToken"]
    );

    // Send 200 `ok`
    ctx.send(data);
  }
};
