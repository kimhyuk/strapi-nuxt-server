"use strict";

const _ = require("lodash");
const axios = require("axios");
const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const shortid = require("shortid");
const me_endpoint_base_url = "https://graph.accountkit.com/v1.1/me";
const token_exchange_base_url =
  "https://graph.accountkit.com/v1.1/access_token";
const app_id = "400446840583468";
const app_secret = "14bb43bc28383144dedc6ef213c8ec13";
/**
 * Read the documentation () to implement custom controller functions
 */

const checkPhone = async code => {
  // CSRF check

  try {
    var app_access_token = ["AA", app_id, app_secret].join("|");
    var params = {
      grant_type: "authorization_code",
      code: code,
      access_token: app_access_token
    };
    // exchange tokens
    let response = await axios.get(token_exchange_base_url, {
      params
    });

    let view = {
      user_access_token: response.data.access_token,
      expires_at: response.data.expires_at,
      user_id: response.data.id
    };

    response = await axios.get(me_endpoint_base_url, {
      params: {
        access_token: view.user_access_token
      }
    });

    view.phone_num = response.data.phone.number;

    return Promise.resolve(response.data.phone.number);
  } catch (err) {
    throw err.response.data.error;
  }
};

module.exports = {
  register: async ctx => {
    const pluginStore = await strapi.store({
      environment: "",
      type: "plugin",
      name: "users-permissions"
    });

    const settings = await pluginStore.get({
      key: "advanced"
    });

    if (!settings.allow_register) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: "Auth.advanced.allow_register" }] }]
          : "Register action is currently disabled."
      );
    }

    const params = _.assign(ctx.request.body, {
      provider: "local"
    });

    if (params.referral) {
      const referral = await strapi.plugins["users-permissions"]
        .queries("user", "users-permissions")
        .findOne({ referralCode: params.referral });

      if (!referral) {
        return ctx.badRequest(
          null,
          ctx.request.admin
            ? [
                {
                  messages: [{ id: "Auth.form.error.referral.notFound" }]
                }
              ]
            : "referral not Found."
        );
      }
    }

    // Password is required.
    if (!params.password) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: "Auth.form.error.password.provide" }] }]
          : "Please provide your password."
      );
    }

    // Throw an error if the password selected by the user
    // contains more than two times the symbol '$'.
    if (
      strapi.plugins["users-permissions"].services.user.isHashed(
        params.password
      )
    ) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: "Auth.form.error.password.format" }] }]
          : "Your password cannot contain more than three times the symbol `$`."
      );
    }

    const role = await strapi.plugins["users-permissions"]
      .queries("role", "users-permissions")
      .findOne({ type: settings.default_role }, []);

    if (!role) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: "Auth.form.error.role.notFound" }] }]
          : "Impossible to find the default role."
      );
    }

    // Check if the provided email is valid or not.
    const isEmail = emailRegExp.test(params.email);

    if (isEmail) {
      params.email = params.email.toLowerCase();
    }

    params.role = role._id || role.id;
    params.password = await strapi.plugins[
      "users-permissions"
    ].services.user.hashPassword(params);

    const user = await strapi.plugins["users-permissions"]
      .queries("user", "users-permissions")
      .findOne({
        email: params.email
      });

    if (user && user.provider === params.provider) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: "Auth.form.error.email.taken" }] }]
          : "Email is already taken."
      );
    }

    if (user && user.provider !== params.provider && settings.unique_email) {
      return ctx.badRequest(
        null,
        ctx.request.admin
          ? [{ messages: [{ id: "Auth.form.error.email.taken" }] }]
          : "Email is already taken."
      );
    }

    try {
      if (!settings.email_confirmation) {
        params.confirmed = true;
      }

      params.phone = await checkPhone(ctx.request.body.code).catch(err => {
        ctx.badRequest(
          null,
          ctx.request.admin
            ? [
                {
                  messages: [{ id: "Auth.form.error.facebook.taken" }]
                }
              ]
            : err.message
        );
      });

      const phoneUser = await strapi.plugins["users-permissions"]
        .queries("user", "users-permissions")
        .findOne({
          phone: params.phone
        });

      if (phoneUser) {
        return ctx.badRequest(
          null,
          ctx.request.admin
            ? [
                {
                  messages: [{ id: "Auth.form.error.phone.taken" }]
                }
              ]
            : "phoneNumber is already taken."
        );
      }

      while (true) {
        params.referralCode = shortid.generate().toUpperCase();

        const u = await strapi.plugins["users-permissions"]
          .queries("user", "users-permissions")
          .findOne({ referralCode: params.referralCode });

        if (!u) break;
      }

      const user = await strapi.plugins["users-permissions"]
        .queries("user", "users-permissions")
        .create(params);

      const jwt = strapi.plugins["users-permissions"].services.jwt.issue(
        _.pick(user.toJSON ? user.toJSON() : user, ["_id", "id"])
      );

      if (settings.email_confirmation) {
        const storeEmail =
          (await pluginStore.get({
            key: "email"
          })) || {};

        const settings = storeEmail["email_confirmation"]
          ? storeEmail["email_confirmation"].options
          : {};

        settings.message = await strapi.plugins[
          "users-permissions"
        ].services.userspermissions.template(settings.message, {
          URL: new URL(
            "/auth/email-confirmation",
            strapi.config.url
          ).toString(),
          USER: _.omit(user.toJSON ? user.toJSON() : user, [
            "password",
            "resetPasswordToken",
            "role",
            "provider"
          ]),
          CODE: jwt
        });

        settings.object = await strapi.plugins[
          "users-permissions"
        ].services.userspermissions.template(settings.object, {
          USER: _.omit(user.toJSON ? user.toJSON() : user, [
            "password",
            "resetPasswordToken",
            "role",
            "provider"
          ])
        });

        try {
          // Send an email to the user.
          await strapi.plugins["email"].services.email.send({
            to: (user.toJSON ? user.toJSON() : user).email,
            from:
              settings.from.email && settings.from.name
                ? `"${settings.from.name}" <${settings.from.email}>`
                : undefined,
            replyTo: settings.response_email,
            subject: settings.object,
            text: settings.message,
            html: settings.message
          });
        } catch (err) {
          return ctx.badRequest(null, err);
        }
      }

      ctx.send({
        jwt,
        user: _.omit(user.toJSON ? user.toJSON() : user, [
          "password",
          "resetPasswordToken"
        ])
      });
    } catch (err) {
      const adminError = _.includes(err.message, "username")
        ? "Auth.form.error.username.taken"
        : "Auth.form.error.email.taken";

      ctx.badRequest(
        null,
        ctx.request.admin ? [{ messages: [{ id: adminError }] }] : err.message
      );
    }
  }
};
